﻿
namespace DesafioPOO
{
    partial class Ejercicio2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtIngresoVotos = new System.Windows.Forms.TextBox();
            this.btnResultados = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.listVotos = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtVotos1 = new System.Windows.Forms.TextBox();
            this.txtPorcentaje1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtVotos2 = new System.Windows.Forms.TextBox();
            this.txtPorcentaje2 = new System.Windows.Forms.TextBox();
            this.txtVotos3 = new System.Windows.Forms.TextBox();
            this.txtPorcentaje3 = new System.Windows.Forms.TextBox();
            this.txtVotos4 = new System.Windows.Forms.TextBox();
            this.txtPorcentaje4 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblAviso = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtIngresoVotos
            // 
            this.txtIngresoVotos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtIngresoVotos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIngresoVotos.Location = new System.Drawing.Point(183, 50);
            this.txtIngresoVotos.Name = "txtIngresoVotos";
            this.txtIngresoVotos.Size = new System.Drawing.Size(243, 20);
            this.txtIngresoVotos.TabIndex = 0;
            this.txtIngresoVotos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIngresoVotos_KeyPress);
            // 
            // btnResultados
            // 
            this.btnResultados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResultados.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResultados.Location = new System.Drawing.Point(456, 32);
            this.btnResultados.Name = "btnResultados";
            this.btnResultados.Size = new System.Drawing.Size(166, 56);
            this.btnResultados.TabIndex = 1;
            this.btnResultados.Text = "Calcular Resultados";
            this.btnResultados.UseVisualStyleBackColor = true;
            this.btnResultados.Click += new System.EventHandler(this.btnResultados_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ingrese los votos:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // listVotos
            // 
            this.listVotos.Cursor = System.Windows.Forms.Cursors.No;
            this.listVotos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listVotos.FormattingEnabled = true;
            this.listVotos.ItemHeight = 20;
            this.listVotos.Location = new System.Drawing.Point(26, 108);
            this.listVotos.Name = "listVotos";
            this.listVotos.Size = new System.Drawing.Size(120, 224);
            this.listVotos.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(179, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Candidato 1:";
            // 
            // txtVotos1
            // 
            this.txtVotos1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVotos1.Location = new System.Drawing.Point(316, 127);
            this.txtVotos1.Name = "txtVotos1";
            this.txtVotos1.ReadOnly = true;
            this.txtVotos1.Size = new System.Drawing.Size(121, 27);
            this.txtVotos1.TabIndex = 5;
            this.txtVotos1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPorcentaje1
            // 
            this.txtPorcentaje1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPorcentaje1.Location = new System.Drawing.Point(501, 127);
            this.txtPorcentaje1.Name = "txtPorcentaje1";
            this.txtPorcentaje1.ReadOnly = true;
            this.txtPorcentaje1.Size = new System.Drawing.Size(121, 27);
            this.txtPorcentaje1.TabIndex = 6;
            this.txtPorcentaje1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(348, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Votos:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(515, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Porcentaje:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(179, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "Candidato 2:";
            // 
            // txtVotos2
            // 
            this.txtVotos2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVotos2.Location = new System.Drawing.Point(316, 187);
            this.txtVotos2.Name = "txtVotos2";
            this.txtVotos2.ReadOnly = true;
            this.txtVotos2.Size = new System.Drawing.Size(121, 27);
            this.txtVotos2.TabIndex = 12;
            this.txtVotos2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPorcentaje2
            // 
            this.txtPorcentaje2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPorcentaje2.Location = new System.Drawing.Point(501, 187);
            this.txtPorcentaje2.Name = "txtPorcentaje2";
            this.txtPorcentaje2.ReadOnly = true;
            this.txtPorcentaje2.Size = new System.Drawing.Size(121, 27);
            this.txtPorcentaje2.TabIndex = 13;
            this.txtPorcentaje2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtVotos3
            // 
            this.txtVotos3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVotos3.Location = new System.Drawing.Point(316, 247);
            this.txtVotos3.Name = "txtVotos3";
            this.txtVotos3.ReadOnly = true;
            this.txtVotos3.Size = new System.Drawing.Size(121, 27);
            this.txtVotos3.TabIndex = 15;
            this.txtVotos3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPorcentaje3
            // 
            this.txtPorcentaje3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPorcentaje3.Location = new System.Drawing.Point(501, 247);
            this.txtPorcentaje3.Name = "txtPorcentaje3";
            this.txtPorcentaje3.ReadOnly = true;
            this.txtPorcentaje3.Size = new System.Drawing.Size(121, 27);
            this.txtPorcentaje3.TabIndex = 16;
            this.txtPorcentaje3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtVotos4
            // 
            this.txtVotos4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVotos4.Location = new System.Drawing.Point(316, 305);
            this.txtVotos4.Name = "txtVotos4";
            this.txtVotos4.ReadOnly = true;
            this.txtVotos4.Size = new System.Drawing.Size(121, 27);
            this.txtVotos4.TabIndex = 17;
            this.txtVotos4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPorcentaje4
            // 
            this.txtPorcentaje4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPorcentaje4.Location = new System.Drawing.Point(501, 305);
            this.txtPorcentaje4.Name = "txtPorcentaje4";
            this.txtPorcentaje4.ReadOnly = true;
            this.txtPorcentaje4.Size = new System.Drawing.Size(121, 27);
            this.txtPorcentaje4.TabIndex = 18;
            this.txtPorcentaje4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(179, 252);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 20);
            this.label8.TabIndex = 23;
            this.label8.Text = "Candidato 3:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(179, 310);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 20);
            this.label11.TabIndex = 26;
            this.label11.Text = "Candidato 4:";
            // 
            // lblAviso
            // 
            this.lblAviso.AutoSize = true;
            this.lblAviso.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAviso.ForeColor = System.Drawing.Color.Red;
            this.lblAviso.Location = new System.Drawing.Point(22, 357);
            this.lblAviso.Name = "lblAviso";
            this.lblAviso.Size = new System.Drawing.Size(16, 20);
            this.lblAviso.TabIndex = 27;
            this.lblAviso.Text = "-";
            // 
            // Ejercicio2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 414);
            this.Controls.Add(this.lblAviso);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtPorcentaje4);
            this.Controls.Add(this.txtVotos4);
            this.Controls.Add(this.txtPorcentaje3);
            this.Controls.Add(this.txtVotos3);
            this.Controls.Add(this.txtPorcentaje2);
            this.Controls.Add(this.txtVotos2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPorcentaje1);
            this.Controls.Add(this.txtVotos1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listVotos);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnResultados);
            this.Controls.Add(this.txtIngresoVotos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Ejercicio2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ejercicio2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtIngresoVotos;
        private System.Windows.Forms.Button btnResultados;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listVotos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtVotos1;
        private System.Windows.Forms.TextBox txtPorcentaje1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtVotos2;
        private System.Windows.Forms.TextBox txtPorcentaje2;
        private System.Windows.Forms.TextBox txtVotos3;
        private System.Windows.Forms.TextBox txtPorcentaje3;
        private System.Windows.Forms.TextBox txtVotos4;
        private System.Windows.Forms.TextBox txtPorcentaje4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblAviso;
    }
}