﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesafioPOO
{
    public partial class Ejercicio1 : Form
    {
        public Ejercicio1()
        {
            InitializeComponent();
        }

        private void Ejercicio1_Load(object sender, EventArgs e)
        {

        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            string num1, num2, num3;
            double a, b, c;
            decimal x1, x2;

            num1 = txtA.Text;
            num2 = txtB.Text;
            num3 = txtC.Text;

            //validación de campos vacíos
            if (num1 != "" && num2 != "" && num3 != "")
            {
                if (Convert.ToDouble(num1) != 0 && Convert.ToDouble(num2) != 0 && Convert.ToDouble(num3) != 0)
                {
                    //cadena de expresiones regulares donde solo se aceptan números
                    Regex regex = new Regex(@"^[0-9]+$");

                    //validación si el campo contiene solo número comparando el campo con la expresión regular
                    if (regex.IsMatch(num1) && regex.IsMatch(num2) && regex.IsMatch(num3))
                    {
                        //cambio de string a double
                        a = double.Parse(num1);
                        b = double.Parse(num2);
                        c = double.Parse(num3);

                        double Error = (Math.Pow(b, 2) - (4 * a * c));
                        
                        if (Error >= 0)
                        {                           
                            //calculo de la formula cuadratica más convirtiendo a decimal
                            x1 = System.Convert.ToDecimal((-b + (Math.Sqrt(Math.Pow(b, 2) - 4 * a * c))) / (2 * a));
                            x2 = System.Convert.ToDecimal((-b - (Math.Sqrt(Math.Pow(b, 2) - 4 * a * c))) / (2 * a));

                            //muestro el resultado redondeado a 2 decimales
                            txtX1.Text = decimal.Round(x1, 2).ToString();
                            txtX2.Text = decimal.Round(x2, 2).ToString();
                            lblAviso.Text = "-";
                        }
                        else
                        {
                            lblAviso.Text = "¡Raíz indefinida, ingrese otro datos!";
                            txtA.Clear();
                            txtB.Clear();
                            txtC.Clear();
                        }
                    }
                    else
                    {
                        lblAviso.Text = "¡Solo se permiten números!";
                    }
                }
                else
                {
                    lblAviso.Text = "¡No se permite el cero!";
                }
            }
            else
            {
                lblAviso.Text = "¡Rellene los campos vacíos!";
            }
        }
    }
}
