﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesafioPOO
{
    public partial class Ejercicio2 : Form
    {
        public Ejercicio2()
        {
            InitializeComponent();
        }

        private void txtIngresoVotos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtIngresoVotos.Text != "")
            {
                //cadena de expresiones regulares donde solo se aceptan números del 1 al 4 separados por coma
                Regex regex = new Regex(@"^[1-4,]+$");


                //validación si el campo contiene solo número comparando el campo con la expresión regular
                if (regex.IsMatch(txtIngresoVotos.Text))
                {
                    //al presionar el enter añade lo del textbox al listbox
                    if (e.KeyChar == (int)Keys.Enter)
                    {
                        //declaro un caracter a eliminar
                        char separador = ',';
                        string secuencia = txtIngresoVotos.Text;
                        //guardando cada número ingresado eliminando el separador (la coma) con split
                        string[] votos = secuencia.Split(separador);

                        //por cada valor en el arreglo lo agrega al listbox
                        foreach (string value in votos)
                        {
                            listVotos.Items.Add(value);
                            txtIngresoVotos.Clear();
                            txtIngresoVotos.Focus();
                        }

                        txtIngresoVotos.Clear();
                    }
                    else
                    {
                        lblAviso.Text = "¡Presiona enter para ingresar los datos!";
                    }
                }
                else
                {
                    lblAviso.Text = "¡Solo permiten números del 1 al 4 separados por coma!";
                }
            }
            else
            {
                lblAviso.Text = "¡Digita algo en el campo vacío!";
            }
        }

        private void btnResultados_Click(object sender, EventArgs e)
        {
            int can1, can2, can3, can4;
            double porce1, porce2, porce3, porce4 = 0;
            string items = "";

            if (listVotos.Items.Count != 0)
            {
                //_____________________Candidado 1
                //Cantidad de votos
                //por cada valor en el listbox suma
                foreach (var item in listVotos.Items)
                {
                    //string que extrae los valores del listbox sin espacios
                    items += item.ToString() + "";
                }
                //Regex.Mathes recorre el string buscando coincidor el indice del candidato
                can1 = Regex.Matches(items, "1").Count / 1;
                txtVotos1.Text = can1.ToString();
                //Porcentaje
                //aplicando regla de y redondeando porcentaje (a*b)/c
                porce1 = can1 * 100;
                txtPorcentaje1.Text = Math.Round(porce1 / listVotos.Items.Count, 2).ToString() + "%";
                lblAviso.Text = "-";



                //_____________________Candidado 2
                foreach (var item in listVotos.Items)
                {
                    items += item.ToString() + "";
                }
                can2 = Regex.Matches(items, "2").Count / 2;
                txtVotos2.Text = can2.ToString();
                //Porcentaje
                porce2 = can2 * 100;
                txtPorcentaje2.Text = Math.Round(porce2 / listVotos.Items.Count, 2).ToString() + "%";
                lblAviso.Text = "-";



                //_____________________Candidado 3
                foreach (var item in listVotos.Items)
                {
                    items += item.ToString() + "";
                }
                can3 = Regex.Matches(items, "3").Count / 3;
                txtVotos3.Text = can3.ToString();
                //Porcentaje
                porce3 = can3 * 100;
                txtPorcentaje3.Text = Math.Round(porce3 / listVotos.Items.Count, 2).ToString() + "%";
                lblAviso.Text = "-";



                //_____________________Candidado 4
                foreach (var item in listVotos.Items)
                {
                    items += item.ToString() + "";
                }
                can4 = Regex.Matches(items, "4").Count / 4;
                txtVotos4.Text = can4.ToString();
                //Porcentaje
                porce4 = can4 * 100;
                txtPorcentaje4.Text = Math.Round(porce4 / listVotos.Items.Count, 2).ToString() + "%";
                lblAviso.Text = "-";
            }
            else
            {
                lblAviso.Text = "¡Lista vacía, dijiste en el campo de arriba!";
            }

            

                        
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
