﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace DesafioPOO
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();            
        }

        private void btnEjercicio2_Click(object sender, EventArgs e)
        {
            Ejercicio2 Ejercicio2 = new Ejercicio2();
            Ejercicio2.Show();
        }

        private void btnEjercicio3_Click(object sender, EventArgs e)
        {
            Ejercicio3 Ejercicio3 = new Ejercicio3();
            Ejercicio3.Show();
        }

        private void btnEjercicio1_Click(object sender, EventArgs e)
        {
            Ejercicio1 Ejercicio1 = new Ejercicio1();
            Ejercicio1.Show();
        }

        private void btnEjercicio4_Click(object sender, EventArgs e)
        {
            Ejer4 ejercicio4 = new Ejer4();
            ejercicio4.Show();
        }
    }
}
